//
//  SecondViewController.swift
//  DairyUsingCoreData
//
//  Created by Mounika Nerella on 9/9/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

//    var newImage = UIImage()
//    var tableViewDetails: Details? = nil
//    var didEdit: Bool = false
//    var path: String = ""
//    weak var delegate: whenEntryDone?
    
    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var descriptlabel: UILabel!
    @IBOutlet weak var titlelabel: UILabel!
    var thoughtObj: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assignNames()
        
        // Do any additional setup after loading the view.
    }
    
    func assignNames(){
        do{
            titlelabel.text = thoughtObj?.lastName
            print(titlelabel.text)
            descriptlabel.text = thoughtObj?.descript
            print(descriptlabel.text)
            let docDirUrl = try ManagingFiles.getDocumentDirectoryUrl()
            print(docDirUrl)
            let imgeUrl = docDirUrl.appendingPathComponent("\(thoughtObj?.timeStamp ?? "")")
            print(imgeUrl)
            DispatchQueue(label: "dq2").async {
                if let imgData = NSData(contentsOf: imgeUrl) {
                    self.myImage.image = UIImage(data: imgData as Data)
                }
            }
        }
        catch{
        }
    }
//    func gettingImage(){
//        var pathImage = path
//        pathImage.removeSubrange(pathImage.range(of: ".plist")!)
//        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
//        let nsUserDomain = FileManager.SearchPathDomainMask.userDomainMask
//        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomain, true)
//        if let dirPath = paths.first
//        {
//            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("Images/\(pathImage).png")
//            self.myImage.image = UIImage(contentsOfFile: imageURL.path)
//        }
//    }

    @IBAction func previousPage(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
