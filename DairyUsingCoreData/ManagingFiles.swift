//
//  ManagingFiles.swift
//  DairyUsingCoreData
//
//  Created by Mounika Nerella on 9/9/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
class ManagingFiles{
    static func getDocumentDirectoryUrl() -> URL{
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectoryUrl = urls.first
        print(documentDirectoryUrl!)
        return documentDirectoryUrl!
    }//
    
    static func createnewFileInUrl(_ newFileName: String,_url: URL) -> URL{
        
        
           let  newFileUrl = getDocumentDirectoryUrl().appendingPathComponent(newFileName)
            print(newFileUrl)
        return newFileUrl
        }
}
   // let newPath = newFileUrl?.appendingPathComponent(newFileName)
//        var myArrayValues: [String: String] = ["timeStamp": values.timeStamp!,"imagefirstName": values.imageName!]
       // FileManager.default.createFile(atPath: (newPath?.path)!, contents: nil, attributes: nil)
//        let theDictionary = NSMutableDictionary(dictionary: myArrayValues)
//        theDictionary.write(to: newFileUrl!, atomically: true)
    //
//    static func readingDataFromPath(path: String,completionHandler: @escaping(NSMutableDictionary) -> Void){
//        let queue = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent )
//        queue.sync {
//            let plistPath = NSMutableDictionary(contentsOf: getDocumentDirectoryUrl().appendingPathComponent(path))
//            completionHandler(plistPath!)
//        }
//    }
//    static func readPlist(_ newFileName: String) throws -> Details?{
//        guard let plistArray = try NSMutableDictionary(contentsOf: getDocumentDirectoryUrl().appendingPathComponent(newFileName))
//        
//            else{
//            return nil
//        }
//        let details = Details(imge: plistArray["imagefirstName"] as! String, tstamp: plistArray["timeStamp"] as! String)
//        print(details)
//        print(plistArray)
//        return details
//    }
//    static func readFileFromPath(completionHandler: @escaping([String]) -> Void){
//        let q = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent)
//        q.async {
//            var pathArray = [String]()
//            do{
//                let dataRead = try
//                FileManager.default.contentsOfDirectory(atPath: getDocumentDirectoryUrl().path)
//                for value in dataRead {
//                    if value.contains(".plist"){
//                        pathArray.append(value)
//                    }
//                }
//                
//            }catch{}
//            completionHandler(pathArray)
//        }
//    }
//    static func writeToFile(val: String,path: String,completionHandler: @escaping() -> Void) {
//        let q = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent)
//        q.async {
//            let dict = NSMutableDictionary(contentsOf: getDocumentDirectoryUrl().appendingPathComponent(path))
//            dict?["timeStamp"] = val
//            dict?.write(to: getDocumentDirectoryUrl().appendingPathComponent(path), atomically: true)
//            completionHandler()
//    }
//    }
//    
//    static func readImageFromPath(path: String,completionHandlerForImage: @escaping(NSData) -> Void){
//        let queue = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent)
//        queue.async {
//        
//            do{
//                let plistDict = try NSData(contentsOf: getDocumentDirectoryUrl().appendingPathComponent("Images/" + path),options: [])
//                completionHandlerForImage(plistDict)
//            }catch{
//                print(error)
//            }
//            
//        }
//    }
//    static func addDataToFile(dairy: Details,path: String){
//        let plistDocumentUrl = getDocumentDirectoryUrl().appendingPathComponent(path)
//        FileManager.default.createFile(atPath: (plistDocumentUrl.path), contents: nil, attributes: nil)
//        let elements: [String: String] = ["timeStamp": dairy.timeStamp!,"imagefirstname": dairy.imageName!]
//        let plistDictionary = NSMutableDictionary(dictionary: elements)
//        plistDictionary.write(to: plistDocumentUrl, atomically: true)
//    }
//    
//    static func addingImageToFile(path: String,imageData: Data,completionHandler: @escaping() -> Void) {
//        let queue = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent)
//        var imageName = String(format: "%@.png", path)
//        queue.async {
//            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
//            let dataPath = documentsDirectory?.appendingPathComponent("Images")
//            do{
//                try FileManager.default.createDirectory(atPath: (dataPath?.path)!, withIntermediateDirectories: true, attributes: nil)
//            }catch let error as NSError {
//               print("Error creating Directory: \(error.localizedDescription)")
//            }
//            let imageUrl = dataPath?.appendingPathComponent(imageName)
//            FileManager.default.createFile(atPath: (imageUrl?.path)!, contents: imageData, attributes: nil)
//            completionHandler()
//        }
//        
//    }
//    
//    static func deleteImage(path: String,completionHandler: @escaping() -> Void) {
//        let queue = DispatchQueue(label: "DairyUsingCoreData",attributes: .concurrent)
//        queue.async {
//            var entry = path
//            do{
//                try FileManager.default.removeItem(at: getDocumentDirectoryUrl().appendingPathComponent(path))
//                entry.removeSubrange(entry.range(of: ".plist")!)
//                try FileManager.default.removeItem(at: getDocumentDirectoryUrl().appendingPathComponent("Images/" + entry))
//            }catch{
//                print(error)
//            }
//            completionHandler()
//        }
//    }
//    static func myPlistFilesAsUrl(_ url: URL) throws -> [String]{
//        var fileName = [String] ()
//        do{
//            var subPath : [String]
//            subPath = try FileManager.default.subpaths(atPath: url.path)!
//            for files in subPath {
//                if files.contains(".plist"){
//                    fileName.append(files)
//                }
//            }
//        
//        }
//        return fileName
//    }

