//
//  EnteringDetailsViewController.swift
//  DairyUsingCoreData
//
//  Created by Mounika Nerella on 9/8/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import CoreData
//protocol  addentry : class{
//    func addNewEntry(fileName: String, data: String)
//}

class EnteringDetailsViewController: UIViewController,UIApplicationDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
   // weak var delegate : addentry?
    var file: String = ""
    var str : String = ""
     var timeStamp : String?
    @IBOutlet weak var imgView: UIImageView!
       @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var descriptInfo: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeStamp = getTimeStamp()
    }
    

    @IBAction func pickImgBtn(_ sender: UIButton) {
       let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
       
    }
    func getTimeStamp() -> String{
        let ts = Date().timeIntervalSince1970
        return String(ts)
    }
    
    
    func getTime()-> String{
        let date = Date()
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.string(from: date)
        return time
        
    }
    
    func getdate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM YYYY"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgView.image = img
        dismiss(animated: true, completion: nil)
    }
//    func convertingImage(){
//        if imgView.image != nil{
//            var data: Data
//            data = UIImageJPEGRepresentation(imgView.image!, 1.5)!
//            print(file)
//            file.removeSubrange(file.range(of: ".plist")!)
//            ManagingFiles.addingImageToFile(path: file, imageData: data as Data, completionHandler: { 
//                DispatchQueue.main.async {
//                    self.delegate?.addNewEntry(fileName: self.firstNameTF.text!, data: self.str)
//                    self.dismiss(animated: true, completion: nil)
//                }
//            })
//        }
//    }
//    func addingallInfo(timeStamp: String,imageName: String){
//        
//    file = timeStamp + ".plist"
//        let theInfo = Details(imge:imageName , tstamp: timeStamp)
//        ManagingFiles.addDataToFile(dairy: theInfo, path: file)
//        DispatchQueue.main.async {
//            self.convertingImage()
//        }
//        
//    }

    @IBAction func addInfo(_ sender: UIButton) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let obj = NSEntityDescription.insertNewObject(forEntityName: "Person", into: appDelegate.persistentContainer.viewContext) as! Person
        DataManager.createNewObject(thought: ThoughtModel(timeStamp: timeStamp, date: getdate(), title: lastNameTF.text, time: getTime(), details: descriptInfo.text))
        do{
            let docDirUrl = try ManagingFiles.getDocumentDirectoryUrl()
            if let img = imgView.image {
                let imgData = UIImagePNGRepresentation(img)
                print(docDirUrl)
                let imageUrl = ManagingFiles .createnewFileInUrl("\(timeStamp ?? " 11111")", _url: docDirUrl)
                DispatchQueue (label: "dq5").async {
                    do{
                        try imgData?.write(to: imageUrl)
                    }catch {
                        print("error")
                    }
                }
            }
        }catch{}
        
//        obj.firstName = firstNameTF.text
//        obj.lastName = lastNameTF.text
//        obj.descript = descriptInfo.text
//       appDelegate.saveContext()
        if   lastNameTF.text != "" && imgView.image != nil{
          //  addingallInfo(timeStamp:firstNameTF.text! , imageName: lastNameTF.text!)
        }else{
            let alertController = UIAlertController(title: "ok", message: "please fill all the fields", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
        navigationController?.popViewController(animated: true)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
