//
//  ViewController.swift
//  DairyUsingCoreData
//
//  Created by Mounika Nerella on 9/8/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate {
    
    var context : NSManagedObjectContext!{
        return DataManager.persistentContainer.viewContext
    }
    
    @IBOutlet weak var tableView: UITableView!
    var personInfo : [Person] = []
  //  @IBOutlet weak var tbleView: UITableView!
    let appdelegate = UIApplication.shared.delegate as? AppDelegate
//    var context: NSManagedObjectContext! {
//        return appdelegate?.persistentContainer.viewContext
//    }
    
    let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
    var frc : NSFetchedResultsController<Person>? = nil
    var dayCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        frc?.delegate = self
        try? frc?.performFetch()
        setUpDate(date: Date())
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func performfetch(){
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp",ascending: false)]
        frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        frc?.delegate = self
        do{
            try frc?.performFetch()
        }
        catch{}
    }
    
    func setUpDate(date: Date) {
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let numberDay = dateFormatter.string(from: date)
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        let monthYear = dateFormatter.string(from: date)
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let textDay = dateFormatter.string(from: date)
        performfetch()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableView.reloadData()
    }
    

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let fetchRequestController = frc {
            return (frc?.sections!.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = self.frc?.sections else{
            fatalError("No sections in fetched result")
        }
        
        let sectionInfo = sections[section]
        

        return sectionInfo.numberOfObjects
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?){
        tableView.reloadData()
    }
    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"DetailsShownViewCell" , for: indexPath) as? DetailsShownViewCell
        
        guard let person = self.frc?.object(at: indexPath) else{
            fatalError("Attempt to configure cell without a managed object")
        }
        
      cell?.textLabel?.text = person.lastName
        //print(person.firstName)
        print(person.lastName)
        return cell!
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let thought = self.frc?.object(at: indexPath)
            else{
                fatalError("Attempt to configure cell without a managed object")
                
        }
        if let controller = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
            controller.thoughtObj = thought
            navigationController?.pushViewController(controller, animated: true)
        }
    }
   // func getData(){
        //     let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        //        do{
        //           personInfo = try context.fetch(Person.fetchRequest())
        //        }
        //        catch{
        //            print("Fetching failed")
        //        }
        //       
        //    }

//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        if let controller = storyBoard.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController {
//            var dict = NSMutableDictionary()
//            var obj : [Details]?
//            print(obj)
//            var pathString: String?
//            print(pathString)
//            ManagingFiles.readingDataFromPath(path: pathString!,  completionHandler: { (mutDict) in
//                dict = mutDict
//                obj = [Details(imge: dict["Image"] as! String, tstamp: dict["timeStamp"] as! String)]
//                let cell = tableView.dequeueReusableCell(withIdentifier:"DetailsShownViewCell" , for: indexPath) as? DetailsShownViewCell
//                guard let person = self.frc?.object(at: indexPath) else{
//                    fatalError("Attempt to configure cell without a managed object")
//                    
//                }
//                cell?.textLabel?.text = "\(person.firstName!) \(person.lastName!)"
//            })
//        }
    //    func addNewEntry(fileName: String,data: String){
    //        gettingPathNames()
    //        tableView.reloadData()
    //    }
    //   // var dairyInfo = [Details]()
    //    var pathArray = [String]()
    //    var resultsArray = [String]()
   
    //    func gettingPathNames(){
    //        pathArray = [String]()
    //        ManagingFiles.readFileFromPath { (pathArray) in
    //            self.pathArray = pathArray
    //            DispatchQueue.main.async {
    //                self.tableView.reloadData()
    //            }
    //        }
    //    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "SecondViewController" {
//            let destinationcontroller = segue.destination as! SecondViewController
//            let indexPath = tableView.indexPathForSelectedRow!
//            //let fname = self.fname[indexPath.row]
//        }
//    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let person = self.frc?.object(at: indexPath) else{
                fatalError("Attempt to configure a cell without a managed object")
            }
            context.delete(person)
            try? context.save()
        }
    }
   

}

