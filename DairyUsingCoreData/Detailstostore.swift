//
//  Detailstostore.swift
//  DairyUsingCoreData
//
//  Created by Mounika Nerella on 9/9/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
import CoreData
import UIKit
struct ThoughtModel {
    var timeStamp: String?
    var date: String?
    //var firstName: String?
    var title: String?
    var time: String?
    var details : String?
}

//class Details{
//    var imageName: String?
//    var timeStamp: String?
//    init(imge: String,tstamp: String) {
//        imageName = imge
//        timeStamp = tstamp
//    }
//}
//    
class DataManager{
//    static func createNewObject(thought: ThoughtModel) {
//        let thoughtObject = NSEntityDescription.insertNewObject(forEntityName: "Person", into: persistentContainer.viewContext)
//    }
    
    static func read() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Person")
        let sortdescriptors = NSSortDescriptor(key: "lastName", ascending: true)
        fetchRequest.sortDescriptors = [sortdescriptors]
        do{
            guard let thought = try persistentContainer.viewContext.fetch(fetchRequest) as? [Person] else{
                return
            }
            for t in thought {
                print(t.lastName ?? "No title",t.descript ?? "No description",t.timeStamp ?? "No ts",t.date ?? "NO date")
            }
            
        }catch{
            
        }
    }
    
    static func createNewObject(thought: ThoughtModel) {
        let thoughtObject = NSEntityDescription.insertNewObject(forEntityName: "Person", into: persistentContainer.viewContext) as! Person
        thoughtObject.timeStamp = thought.timeStamp
        thoughtObject.date = thought.date
        thoughtObject.descript = thought.details
        thoughtObject.lastName = thought.title
        thoughtObject.time = thought.time
        saveContext()
    }
    
    
    //create coredata stack
    static var persistentContainer : NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DairyUsingCoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    //coredata save support
    static func saveContext(){
        let context = persistentContainer.viewContext
        if context .hasChanges {
            do{
                try context.save()
            }catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(error), \(nserror.userInfo)")
            }
        }
    }
}
